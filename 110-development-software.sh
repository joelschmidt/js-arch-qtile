#!/bin/bash
#set -e
###############################################################################
# Author	:	 JS
###############################################################################

###############################################################################
#
#   DECLARATION OF FUNCTIONS
#
###############################################################################

func_install() {
	if pacman -Qi $1 &>/dev/null; then
		tput setaf 2
		echo "###############################################################################"
		echo "################## The package "$1" is already installed"
		echo "###############################################################################"
		echo
		tput sgr0
	else
		tput setaf 3
		echo "###############################################################################"
		echo "##################  Installing package " $1
		echo "###############################################################################"
		echo
		tput sgr0
		sudo pacman -S --noconfirm --needed $1
	fi
}

###############################################################################
echo "Installation of the development packages"
###############################################################################

list=(
	firefox
	qutebrowser
	vivaldi
	chromium
	flameshot
	meld
	neovim
)

count=0

for name in "${list[@]}"; do
	count=$((count + 1))
	tput setaf 3
	echo "Installing package nr.  "$count " " $name
	tput sgr0
	func_install $name
done

paru -S qtile-extras
paru -S meh

git clone https://github.com/LazyVim/starter ~/.config/nvim
git clone https://gitlab.com/joelschmidt/dotfiles.git ~/dotfiles

rm -rf $HOME/.config/qtile

ln -s $HOME/dotfiles/qtile $HOME/.config/

###############################################################################

tput setaf 11
echo "################################################################"
echo "Software has been installed"
echo "################################################################"
echo
tput sgr0
